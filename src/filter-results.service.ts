import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import * as url from 'url';

@Injectable({
  providedIn: 'root'
})
export class FilterResultsService {
  user = 'mikko@girdit.com';
  apiToken = 'Gg6g1nolXWhGtjV7vlGwB909';
  // baseUrl = 'https://girdit.atlassian.net/sr';
  // baseUrl = 'http://localhost:4200/sr';
  baseUrl = '/api/sr';

  constructor(private http: HttpClient) { }

  createAuthorizationToken() {
    const authToken = btoa(this.user + ':' + this.apiToken);
    return authToken;
  }

  createUrl(id: string, exportFormat: string) {
    return this.baseUrl + `/jira.issueviews:searchrequest-${exportFormat}/${id}/SearchRequest-${id}.html?tempMax=1000`;
  }

  getResults(requestId: string, exportFormat: string) {
    const requestUrl = this.createUrl(requestId, exportFormat);
    let headers = new HttpHeaders();
    const token = this.createAuthorizationToken();
    headers = headers.append('Authorization', 'Basic ' + token);
    return this.http.get(requestUrl, {
      headers: headers,
      responseType: 'text'
    });
  }
}
