import { Component } from '@angular/core';
import {FilterResultsService} from '../filter-results.service';
import {HttpErrorResponse} from '@angular/common/http';
import {DomSanitizer, SafeScript} from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {


  constructor(private filterResultsService: FilterResultsService, private sanitizer: DomSanitizer) {}
  filterId = '10005';
  summaryList: SafeScript = '';
  detailedList: SafeScript = '';
  errorDesc = '';

  static removeNodes(dom, selector) {
    const elements = dom.querySelectorAll(selector);
    Array.prototype.forEach.call( elements, function( node ) {
      node.parentNode.removeChild( node );
    });
  }

  static removeJiraTableField(dom, fieldName) {
    AppComponent.removeNodes(dom, '.headerrow-' + fieldName);
    AppComponent.removeNodes(dom, '.' + fieldName);
  }

  static checkIfRowCell0B0(row, value) {
    const cell0 = row.querySelector('td');
    if (cell0 === null) {
      return false;
    }
    const b0 = cell0.querySelector('b');
    if (b0 === null) {
      return false;
    }
    return b0.innerHTML.trim() === value;
  }

  static checkIfStatusTable(rows) {
    if (rows.length !== 7) {
      return false;
    }
    return AppComponent.checkIfRowCell0B0(rows[0], 'Type:');
  }

  static checkIfDetailTable(rows) {
    // check that row[1]->cell0->b0->Status:
    if (rows.length !== 6) {
      return false;
    }

    return AppComponent.checkIfRowCell0B0(rows[1], 'Status:');
  }

  static checkIfLinksTable(rows) {
    // check if tr0->td0-->b0 == Issue links
    if (rows.length < 2) {
      return false;
    }

    return AppComponent.checkIfRowCell0B0(rows[0], 'Issue links:');
  }

  static sanitizeIssueDetails(dom) {
    // table.tableBorder -> tbody -> tr[4,5], check that row[1]->cell0->b0->Status:
    const elements = dom.querySelectorAll('table.tableBorder');
    Array.prototype.forEach.call( elements, function( node ) {
      const tbody = node.querySelector('tbody');
      if (tbody != null) {
        const rows = tbody.querySelectorAll('tr');
        if (AppComponent.checkIfDetailTable(rows)) {
          tbody.removeChild(rows[4]);
          tbody.removeChild(rows[5]);
        }
      }
    });

  }

  static sanitizeIssueStatus(dom) {
    // table.grid -> tbody -> tr[1-6], check that row0->cell0->b0->Type:
    const elements = dom.querySelectorAll('table.grid');
    Array.prototype.forEach.call( elements, function( node ) {
      const tbody = node.querySelector('tbody');
      if (tbody != null) {
        const rows = tbody.querySelectorAll('tr');
        if (AppComponent.checkIfStatusTable(rows)) {
          tbody.removeChild(rows[1]);
          tbody.removeChild(rows[2]);
          tbody.removeChild(rows[3]);
          tbody.removeChild(rows[4]);
          tbody.removeChild(rows[5]);
          tbody.removeChild(rows[6]);
        }
      }
    });
  }

  static removeIssueLinks(dom) {
    // table.grid -> tr0->td0-->b0 == Issue links
    const elements = dom.querySelectorAll('table.grid');
    Array.prototype.forEach.call( elements, function( node ) {
      const tbody = node.querySelector('tbody');
      if (tbody != null) {
        const rows = tbody.querySelectorAll('tr');
        if (AppComponent.checkIfLinksTable(rows)) {
          const parentNode = node.parentElement;
          parentNode.removeChild(node);
        }
      }
    });
  }

  processError(page, error) {
    this.errorDesc += ' ' + page + error.statusText;
  }

  processSummaryData(data) {
    const parser = new DOMParser();
    const dom = parser.parseFromString(data, 'text/html');
    // remove due date, updated, created
    AppComponent.removeJiraTableField(dom, 'duedate');
    AppComponent.removeJiraTableField(dom, 'updated');
    AppComponent.removeJiraTableField(dom, 'created');
    AppComponent.removeNodes(dom, '#previous-view');

    const filteredData = dom.documentElement.outerHTML;
    this.summaryList = this.sanitizer.bypassSecurityTrustHtml(filteredData);
  }

  processDetailedData(data) {
    const parser = new DOMParser();
    const dom = parser.parseFromString(data, 'text/html');
    AppComponent.removeNodes(dom, '#previous-view');
    // sanitize issue details table
    AppComponent.sanitizeIssueDetails(dom);

    // sanitize issue status table
    AppComponent.sanitizeIssueStatus(dom);

    // remove issue links
    AppComponent.removeIssueLinks(dom);

    const filteredData = dom.documentElement.outerHTML;
    this.detailedList = this.sanitizer.bypassSecurityTrustHtml(filteredData);
  }

  loadPage() {
    this.errorDesc = '';
    this.loadSummaryPage();
    this.loadDetailedPage();
  }

  loadSummaryPage() {
    this.getResults(this.filterId, 'summary');
  }

  loadDetailedPage() {
    this.getResults(this.filterId, 'detailed');
  }

  getResults(filterId, page) {
    let exportFormat = 'fullcontent';
    if (page === 'summary') {
      exportFormat = 'printable';
    }
    this.filterResultsService.getResults(filterId, exportFormat).subscribe(
      results => {
        if (page === 'summary') {
          this.processSummaryData(results);
        } else if (page === 'detailed') {
          this.processDetailedData(results);
        }
      },
      error => {
        this.processError(page, error);
      }
    );
  }
}
